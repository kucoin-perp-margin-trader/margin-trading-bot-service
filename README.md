# Margin Trading Bot Service

The heart of the project. This service fetches data from the Kucoin API and creates and cancels margin orders.  
  
Margin Orders are created primarly based on the reading on the MACD/VOL. They are very short trades with very high leverage on the 1-min candles to scrape average-wise relatively safe profits on VERY short trades (1-3min). 20x leverage min, 50x max.  
